// alert('s04');

// let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
// let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
// let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
// let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        this.gradeAve = undefined;
        this.pass = undefined ;
        this.passWithHonors = undefined;
        this.grades = (grades instanceof Array && grades.length === 4 && grades.every(grade => grade >= 0 && grade <= 100)) ? grades : undefined;
    }
    login() {
        console.log('Logging in ' + this.email);
        return this;
    }
    logout() {
        console.log('Logging out ' + this.email);
        return this;
    }
    listGrades() {
        console.log(`${this.name} grades: ${this.grades}`);
        return this;
    }
    computeAve() {
        let sum = 0;
        for (let i = 0; i < this.grades.length; i++) {
            sum += this.grades[i];
        }
        this.gradeAve = sum / this.grades.length;
        return this;
    }
    willPass() {
        this.pass = this.computeAve() >= 85 ? true : false;
        return this;
        
    }
    willPassWithHonors() {
        let value = this.computeAve().gradeAve;
        if (value >= 90) {
            this.passWithHonors = true;
        } else if (value <= 85) {
            this.passWithHonors = false;
        } else {
            this.passWithHonors = undefined;
        }
        return this;
    }
}

class Section {
    // every section object will be instatiated with an empty array for its students.
    constructor(name) {
        this.name = name;
        this.students = [];
        this.honorStudnets = [];
        this.honorPercentage = undefined;
    }
    addStudent(name, email, grades) {
        this.students.push(new Student(name, email, grades));
        return this;
    }

    //method for computing how many students in the section are honor students
    countHonorStudents() {
        let count = 0;
        this.students.forEach(student => {
            if (student.computeAve().willPass().willPassWithHonors().passWithHonors) {
                count++;
            }
        })
        this.honorStudnets = count;
        return this;
    }
    //method for computing the percentage of honor students in the section
    computeHonorsPercentage() {
        this.honorPercentage = this.honorStudnets / this.students.length * 100;
        return this;
    }
    
}

const section1A = new Section('1A');
console.log(section1A);

//invoke the addStudent method on the section1A object while passing in the student's name, email, and grades as arguments.
// section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
// section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
// section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
// console.log(section1A);

//can we still invoke a partiular student's properties and methods via dot notation.
// console.log(section1A.students[0].computeAve());


// section1A.countHonorStudents();
// console.log(section1A);

// create a method that is a replica of computeHonorsPercentage(). 
//Create a property honorPercentage with a value of undefined. 
//create a computeHonorsPercentage() that will compute for the total number of honor students divided by total number of students multiplied by 100. 
// the resulting value will be assigned to the property honorsPercentage. return the instance.

// check computeHonorsPercentage() method
// section1A.computeHonorsPercentage();
// console.log(section1A);

//========FUCNTIONCODING
// #1
// Define a Grade class whose constructor will accept a number argument to serve as its grade level. It will have the following properties: 1) level initialized to passed in number argument, 2) sections initialized to an empty array, 3) totalStudents initialized to zero, 4) totalHonorStudents initialized to zero, 5) batchAveGrade set to undefined, 6) batchMinGrade set to undefined, 7) batchMaxGrade set to undefined.

// #2 Define an addSection() method that will take in a string argument to instantiate a new Section object and push it into the sections array property.

class Grade {
    constructor(level) {
        this.level = level;
        this.sections = [];
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
        this.batchAveGrade = undefined;
        this.batchMinGrade = undefined;
        this.batchMaxGrade = undefined;
    }
    addSection(name) {
        this.sections.push(new Section(name));
        return this;
    }
    //#4 
    // Define a countStudents() method that will iterate over every section in the grade level, incrementing the totalStudents property of the grade level object for every student found in every section.
    countStudents() {
        this.sections.forEach(section => {
            this.totalStudents += section.students.length;
        })
        return this;
    }
    // #5
    // Define a countHonorStudents() method that will perform similarly to countStudents() except that it will only consider honor students when incrementing the totalHonorStudents property.
    countHonorStudents() {
        this.sections.forEach(section => {
            this.totalHonorStudents += section.honorStudnets;
        })
        return this;
    }
    // #6 
    computeBatchAve() {
        let sum = 0;
        this.sections.forEach(section => {
            section.students.forEach(student => {
                sum += student.computeAve().gradeAve;
            })
        })
        this.batchAveGrade = sum / this.totalStudents;
        return this;
    }
    // #7
    getBatchMinGrade() {
        let min = 100;
        this.sections.forEach(section => {
            section.students.forEach(student => {
                if (student.computeAve().gradeAve < min) {
                    min = student.computeAve().gradeAve;
                }
            })
        })
        this.batchMinGrade = min;
        return this;
    }
    // #8
    getBatchMaxGrade() {
        let max = 0;
        this.sections.forEach(section => {
            section.students.forEach(student => {
                if (student.computeAve().gradeAve > max) {
                    max = student.computeAve().gradeAve;
                }
            })
        })
        this.batchMaxGrade = max;
        return this;
    }

}
// check Grade class
const grade1 = new Grade(1);
console.log(grade1);

// check addSection() method
grade1.addSection('1A');
console.log(grade1);

// #3
//Write a statement that will add a student to section1A. The student is named John, with email john@mail.com, and grades of 89, 84, 78, and 88.
grade1.sections.find(section=>section.name === "1A").addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
grade1.sections.find(section=>section.name === "1A").addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
grade1.sections.find(section=>section.name === "1A").addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
console.log(grade1);

// check countStudents() method
console.log(grade1.countStudents());

// check countHonorStudents() method
grade1.sections.forEach(section => {
    section.countHonorStudents();
}) 
console.log(grade1.countHonorStudents());


// check computeBatchAve() method
grade1.computeBatchAve();
console.log(grade1);

// check getBatchMinGrade() method
grade1.getBatchMinGrade();
console.log(grade1);

// check getBatchMaxGrade() method
grade1.getBatchMaxGrade();
console.log(grade1);

